package com.mygov.kisan

import android.app.Application
import android.os.Bundle
import com.google.gson.Gson
import com.mygov.kisan.utils.SharedPrefsManager.Companion.initialize

class AppController : Application() {

    override fun onCreate() {
        super.onCreate()
        initialize(this)
    }


    companion object {
        var mGson: Gson? = null
        private var bundle: Bundle? = null
        @get:Synchronized
        var instance: AppController? = null
            private set

        val gson: Gson?
            get() {
                if (mGson == null) {
                    mGson = Gson()
                }
                return mGson
            }

        fun getBundle(): Bundle? {
            if (bundle == null) {
                bundle = Bundle()
            }
            return bundle
        }
    }



}
