package com.mygov.kisan.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mygov.kisan.R
import com.mygov.kisan.ui.Utils.NetworkCheck
import com.mygov.kisan.ui.model.CropTypes
import com.mygov.kisan.ui.viewmodel.DashboardViewModel
import com.mygov.kisan.utils.Constant.Companion.CROPS_ID
import kotlinx.android.synthetic.main.fragment_dashboard.view.*

class DashboardFragment : Fragment() {
    lateinit var root : View
    lateinit var cropTypesList : List<CropTypes.Result>
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

         root = inflater.inflate(R.layout.fragment_dashboard, container, false)
         initializeProgress()

        if (NetworkCheck.isNetworkAvailable(context)) {
            DashboardViewModel.instance.getCropTypes()
                .observe(viewLifecycleOwner, Observer { cropTypes ->
                    val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
                    this.cropTypesList = cropTypes.result!!

                    Glide.with(requireActivity()).load(cropTypesList.get(0)?.item_image.toString()).into(root.ivCropFirst)
                    Glide.with(requireActivity()).load(cropTypesList.get(1)?.item_image.toString()).into(root.ivCropSecond)
                    Glide.with(requireActivity()).load(cropTypesList.get(2)?.item_image.toString()).into(root.ivCropThird)
                    Glide.with(requireActivity()).load(cropTypesList.get(3)?.item_image.toString()).apply(requestOptions).into(root.ivCropFourth)

                    root.tvFirstCropType.text= cropTypesList.get(0)?.title.toString()
                    root.tvSecondCropType.text= cropTypesList.get(1)?.title.toString()
                    root.tvThirdCropType.text= cropTypesList.get(2)?.title.toString()
                    root.tvFourthCropType.text= cropTypesList.get(3)?.title.toString()

                })
            }

        root.cardCropFirst.setOnClickListener {
            val bundle= bundleOf(CROPS_ID to cropTypesList[0].id.toString())
            findNavController().navigate(R.id.action_crops_type_to_crops_details ,bundle)
        }

        root.cardCropSecond.setOnClickListener {
            val bundle= bundleOf(CROPS_ID to cropTypesList[1].id.toString())
            findNavController().navigate(R.id.action_crops_type_to_crops_details ,bundle)
        }

        root.cardCropThird.setOnClickListener {
            val bundle= bundleOf(CROPS_ID to cropTypesList[2].id.toString())
            findNavController().navigate(R.id.action_crops_type_to_crops_details ,bundle)
        }

        root.cardCropFourth.setOnClickListener {
            val bundle= bundleOf(CROPS_ID to cropTypesList[3].id.toString())
            findNavController().navigate(R.id.action_crops_type_to_crops_details ,bundle)
        }
        return root
    }

    private fun initializeProgress() {
        DashboardViewModel.instance.isLoading.observe(requireActivity(), Observer { isLoading->
            if (isLoading){
                root.progressBar.visibility= View.VISIBLE
                root.cropsConstraints.visibility= View.GONE
            }else{
                root.progressBar.visibility= View.GONE
                root.cropsConstraints.visibility= View.VISIBLE
            }

        })
    }




}