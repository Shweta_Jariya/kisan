package com.mygov.kisan.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mygov.kisan.R
import com.mygov.kisan.ui.Utils.NetworkCheck
import com.mygov.kisan.ui.viewmodel.CropDetailViewModel
import com.mygov.kisan.utils.Constant
import kotlinx.android.synthetic.main.double_bed_plot.view.*
import kotlinx.android.synthetic.main.fragment_samanay_jankari.view.*
import kotlinx.android.synthetic.main.fragment_samanay_jankari.view.progressBar
import kotlinx.android.synthetic.main.single_bed_plot.view.*

class SamanayJankariFragment : Fragment() {
    lateinit var root :View
    var cropId : String?=null
    var cropImage : String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments!=null)
            cropId = arguments?.getString(Constant.CROPS_ID)
            cropImage = arguments?.getString(Constant.CROPS_IMAGE)
          }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle? ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_samanay_jankari, container, false)
        initializeProgress()

        if (NetworkCheck.isNetworkAvailable(context)) {
            /*val adapter = MasterListAdapter(activity, masterList,this)
            root.recyclerViewList.adapter = adapter*/
            Log.e("cropId",cropId.toString())
             apiCallCropData()
        }
        return root
    }

    private fun apiCallCropData() {
      CropDetailViewModel.instance.getCropChart(cropId.toString()).observe(requireActivity(), Observer { cropChart ->
              if (cropChart!=null){
                  val cropChartData = cropChart.result?.plantation_chart?.get(0)
                  val cropEconomics = cropChart.result?.crop_economics?.get(0)
                  val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
                  Glide.with(requireActivity()).load(cropImage).apply(requestOptions).into(root.ivCropImg)

                  root.tvCropName.text= cropChartData?.crop_name
                  root.tvCropTitle.text= cropChartData?.crop_name
                  root.tvAverageExpenditure.text= cropEconomics?.average_expenditure
                  root.tvAverageProduction.text= cropEconomics?.average_production
                  root.tvAverageCostTable.text= cropEconomics?.average_price
                  val gross_income: Float = cropEconomics?.average_price!!.toFloat() * cropEconomics.average_production?.toInt()!!
                  root.tvGrossIncomeTable.text= gross_income.toString()
                  val net_income: Float = gross_income - cropEconomics?.average_expenditure!!.toFloat()
                  root.tvNetIncome.text= net_income.toString()

                  if (cropChartData?.line_pattern?.equals(1)!!){
                      root.lySinglebed.visibility=VISIBLE
                      root.tv_single_distance_between_columns.text= cropChartData.distance_between_columns_cm +" "+context?.getResources()?.getString(R.string.field_unit)
                      root.tv_single_distance_between_plants.text=cropChartData.distance_between_plants_cm +" "+context?.getResources()?.getString(R.string.field_unit)

                  }else{
                       root.lyDoublebed.visibility=VISIBLE
                          root.tv_distance_between_columns.text=cropChartData?.distance_between_columns_cm +" "+context?.getResources()?.getString(R.string.field_unit)
                          root.tv_distance_between_plants.text=cropChartData?.distance_between_plants_cm +" "+context?.getResources()?.getString(R.string.field_unit)
                          root.tv_double_distance_between_rows.text=cropChartData?.distance_between_rows_cm +" "+context?.getResources()?.getString(R.string.field_unit)
                  }

                  if (!cropChartData?.general_info.isNullOrEmpty())
                  {
                      root.tvGeneralInfo.visibility= VISIBLE
                      root.webview.visibility= VISIBLE
                      root.webview.loadDataWithBaseURL(null,
                          cropChartData?.general_info!!, "text/html", "UTF-8", null)
                  }



                  root.tvDistanceBetweenColumns.setText(cropChartData?.distance_between_columns_cm)
                  root.tvDistanceBetweenPlants.setText(cropChartData?.distance_between_plants_cm)
                  root.tvDistanceBetweenRows.setText(cropChartData?.distance_between_rows_cm)
                  root.tvBedSize.setText(cropChartData?.bed_size)
                  if (cropChartData?.ideal_production != "") {
                      root.llIdealProduction.setVisibility(VISIBLE)
                      root.tvIdealProductionValue.text=cropChartData?.ideal_production
                  }
                  if (cropChartData?.distance_between_rows_cm!= "" && cropChartData?.distance_between_rows_cm!= "0") {
                      root.llDistanceBetweenRow.setVisibility(VISIBLE)
                      root.llPodhaRopanVidhi.setVisibility(VISIBLE)
                  }
                  if (cropChartData?.distance_between_plants_cm!= "" && cropChartData?.distance_between_plants_cm != "0") {
                      root.llDistanceBetweenPlant.setVisibility(VISIBLE)
                      root.llPodhaRopanVidhi.setVisibility(VISIBLE)
                  }
                  if (cropChartData?.distance_between_columns_cm!= "" && cropChartData?.distance_between_columns_cm!= "0") {
                      root.llDistanceBetweenColum.setVisibility(VISIBLE)
                      root.llPodhaRopanVidhi.setVisibility(VISIBLE)
                  }
              }

      })
    }

    private fun initializeProgress() {
        CropDetailViewModel.instance.isLoading.observe(requireActivity(), Observer { isLoading->
            if (isLoading){
                root.progressBar.visibility= View.VISIBLE
            }else{
                root.progressBar.visibility= View.GONE
            }

        })
    }
}