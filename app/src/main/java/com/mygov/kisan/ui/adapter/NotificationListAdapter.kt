package com.mygov.kisan.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.mygov.kisan.R
import com.mygov.kisan.ui.model.MasterData


class NotificationListAdapter(
    val context: FragmentActivity?,
    val masterList: List<MasterData.Response?>?,
    private val jobInterface: EducationInterface
): RecyclerView.Adapter<NotificationListAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: NotificationListAdapter.ViewHolder, position: Int) {
      /*val masterData : MasterData.Response = masterList?.get(position)!!


        context?.let {
            Glide.with(it)
                .load(masterData.image)
                .into(holder.ivUserImg)
        }

        holder.tvUserName?.text = masterData.username

          Log.e("user id",""+masterData.user_id)
         holder.tvLocation?.text =  masterData.state+", "+masterData.city
         holder.tvDiscription?.text =  masterData.about_me


        holder.btViewProfile.setOnClickListener {
            jobInterface.onClickViewProfile(masterData)
        }
       *//* holder.recyclerList.layoutManager= LinearLayoutManager(context, RecyclerView.HORIZONTAL,false)
        val adapter =SkillListAdapter(context, masterData.services)
        holder.recyclerList.adapter = adapter*//*

        if (masterData?.is_office_services.equals("1")){
            //holder.llOfficeServices.visibility= View.VISIBLE
            holder.cardOfficeServices.visibility= View.VISIBLE
        }else{
            holder.cardOfficeServices.visibility= View.GONE
        }
        if (masterData?.is_remote_service.equals("1")){
            //holder.llOfferRemoteServices.visibility= View.VISIBLE
            holder.cardOfferRemoteServices.visibility= View.VISIBLE
        }else{
            holder.cardOfferRemoteServices.visibility= View.GONE
        }
        if (masterData?.is_travels_client.equals("1")){
           // holder.llTravelToClient.visibility= View.VISIBLE
            holder.cardTravelToClient.visibility= View.VISIBLE
        }else{
            holder.cardTravelToClient.visibility= View.GONE
        }*/
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        //return masterList?.size!!
        return 5
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivUserImg = itemView.findViewById<ImageView>(R.id.ivUserImg)

        val tvUserName = itemView.findViewById<TextView>(R.id.tvUserName)
        val tvLocation = itemView.findViewById<TextView>(R.id.tvLocation)

        val tvDiscription = itemView.findViewById<TextView>(R.id.tvDiscription)

        val btViewProfile = itemView.findViewById<Button>(R.id.btViewProfile)
        val recyclerList = itemView.findViewById<RecyclerView>(R.id.recyclerList)


        val llOfficeServices = itemView.findViewById<LinearLayout>(R.id.llOfficeServices)
        val cardOfficeServices = itemView.findViewById<CardView>(R.id.cardOfficeServices)

        val llTravelToClient = itemView.findViewById<LinearLayout>(R.id.llTravelToClient)
        val cardTravelToClient = itemView.findViewById<CardView>(R.id.cardTravelToClient)

        val llOfferRemoteServices = itemView.findViewById<LinearLayout>(R.id.llOfferRemoteServices)
        val cardOfferRemoteServices = itemView.findViewById<CardView>(R.id.cardOfferRemoteServices)

    }
    interface EducationInterface{
        fun onClickViewProfile(jobsData: MasterData.Response)
    }
}
