package com.mygov.kisan.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mygov.kisan.R
import com.mygov.kisan.ui.Utils.GridItemDecoration
import com.mygov.kisan.ui.Utils.NetworkCheck
import com.mygov.kisan.ui.adapter.DripNutrigationDetailAdapter
import com.mygov.kisan.ui.adapter.DripNutrigationCropAttributeAdapter
import com.mygov.kisan.ui.viewmodel.CropDetailViewModel
import com.mygov.kisan.utils.Constant
import kotlinx.android.synthetic.main.fragment_drip_nutrigation.view.*
import kotlinx.android.synthetic.main.fragment_drip_nutrigation.view.ivCropImg
import kotlinx.android.synthetic.main.fragment_drip_nutrigation.view.progressBar
import kotlinx.android.synthetic.main.fragment_drip_nutrigation.view.tvCropTitle


class DripNutrigationFragment : Fragment() {
   lateinit var root : View
    var cropId : String?=null
    var cropImage : String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments!=null)
            cropId = arguments?.getString(Constant.CROPS_ID)
        cropImage = arguments?.getString(Constant.CROPS_IMAGE)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root= inflater.inflate(R.layout.fragment_drip_nutrigation, container, false)
        root.rvDripNutrigationCropAttrib.layoutManager= activity?.let { GridLayoutManager(it,3) }
        root.rvDripNutrigationCropAttrib.addItemDecoration(GridItemDecoration(10, 3))
        root.recyclerViewDripNutrigation.layoutManager= activity?.let { LinearLayoutManager(it, RecyclerView.VERTICAL,false) }


        initializeProgress()

        if (NetworkCheck.isNetworkAvailable(context)) {
            /*val adapter = MasterListAdapter(activity, masterList,this)
            root.recyclerViewList.adapter = adapter*/
            Log.e("cropId",cropId.toString())
            apiCallCropDripNutrition()
        }

        return root
    }

    private fun apiCallCropDripNutrition() {
        CropDetailViewModel.instance.getCropsDripNutrigation(cropId).observe(requireActivity(), Observer { cropDripNutrigation ->
            if (cropDripNutrigation!=null){
                val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
                Glide.with(requireActivity()).load(cropImage).apply(requestOptions).into(root.ivCropImg)

                root.tvCropTitle.text= cropDripNutrigation?.result?.crop
                root.tvCropCatTitle.text= resources.getString(R.string.drip)+" | "+cropDripNutrigation?.result?.type
                if (cropDripNutrigation.result?.crop_attribute!=null && cropDripNutrigation.result?.crop_attribute?.size!=0)
                  {
                   val dripAdapter = DripNutrigationCropAttributeAdapter(activity, cropDripNutrigation.result?.crop_attribute)
                root.rvDripNutrigationCropAttrib.adapter= dripAdapter
               }else{
                    root.tvNodata.visibility =VISIBLE
                }
                if (cropDripNutrigation.result?.details!=null && cropDripNutrigation.result?.details?.size!=0)
                {
                    val dripAdapter = DripNutrigationDetailAdapter(activity, cropDripNutrigation.result?.details)
                    root.recyclerViewDripNutrigation.adapter= dripAdapter
                }
                if (!cropDripNutrigation.result?.note.isNullOrEmpty())
                {
                   // root.webView.visibility= VISIBLE
                    root.webView.loadDataWithBaseURL(null,
                        cropDripNutrigation.result?.note!!, "text/html", "UTF-8", null)
                }
            }else{

            }

        })
    }

    private fun initializeProgress() {
        CropDetailViewModel.instance.isLoading.observe(requireActivity(), Observer { isLoading->
            if (isLoading){
                root.progressBar.visibility= View.VISIBLE
            }else{
                root.progressBar.visibility= View.GONE
            }

        })
    }
}