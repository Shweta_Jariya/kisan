package com.mygov.kisan.ui.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CropDripNutrigation {
    @Expose
    @SerializedName("result")
    var result: Result? = null

    @Expose
    @SerializedName("message")
    var message: String? = null

    @Expose
    @SerializedName("status")
    var status = false

    class Result {
        @Expose
        @SerializedName("details")
        var details: List<Details>? =
            null

        @Expose
        @SerializedName("crop_attribute")
        var crop_attribute: List<Crop_attribute>? = null

        @Expose
        @SerializedName("note")
        var note: String? = null

        @Expose
        @SerializedName("image")
        var image: String? = null

        @Expose
        @SerializedName("type")
        var type: String? = null

        @Expose
        @SerializedName("crop")
        var crop: String? = null

    }

    class Details {
        @Expose
        @SerializedName("ca3n2")
        var ca3n2: String? = null

        @Expose
        @SerializedName("fertilizers_map")
        var fertilizers_map: String? = null

        @Expose
        @SerializedName("fertilizers_0_0_50")
        var fertilizers_0_0_50: String? = null

        @Expose
        @SerializedName("fertilizers_13_00_45")
        var fertilizers_13_00_45: String? = null

        @Expose
        @SerializedName("fertilizers_mgso4")
        var fertilizers_mgso4: String? = null

        @Expose
        @SerializedName("fertilizers_mop")
        var fertilizers_mop: String? = null

        @Expose
        @SerializedName("fertilizers_12_61_00")
        var fertilizers_12_61_00: String? = null

        @Expose
        @SerializedName("nutrients_mgo")
        var nutrients_mgo: String? = null

        @Expose
        @SerializedName("nutrients_k")
        var nutrients_k: String? = null

        @Expose
        @SerializedName("nutrients_p")
        var nutrients_p: String? = null

        @Expose
        @SerializedName("nutrients_n")
        var nutrients_n: String? = null

        @Expose
        @SerializedName("white_potas")
        var white_potas: String? = null

        @Expose
        @SerializedName("h3po4")
        var h3po4: String? = null

        @Expose
        @SerializedName("fertilizers_amm_sulphate")
        var fertilizers_amm_sulphate: String? = null

        @Expose
        @SerializedName("fertilizers_urea")
        var fertilizers_urea: String? = null

        @Expose
        @SerializedName("life_cycle")
        var life_cycle: String? = null

        @Expose
        @SerializedName("total_days")
        var total_days = 0

        @Expose
        @SerializedName("end_day")
        var end_day = 0

        @Expose
        @SerializedName("start_day")
        var start_day = 0

    }

    class Crop_attribute {
        @Expose
        @SerializedName("value")
        var value: String? = null

        @Expose
        @SerializedName("title")
        var title: String? = null

    }
}