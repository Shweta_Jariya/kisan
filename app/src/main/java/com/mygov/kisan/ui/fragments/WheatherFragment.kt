package com.mygov.kisan.ui.fragments

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.google.gson.Gson
import com.mygov.kisan.R
import com.mygov.kisan.ui.Utils.DateHelper
import com.mygov.kisan.ui.Utils.WheatherCondition.wheather
import com.mygov.kisan.ui.adapter.WeatherAdapter

import com.mygov.kisan.ui.model.WeatherCelciousData
import com.mygov.kisan.ui.repository.wheather.VolleyRequest
import com.mygov.kisan.ui.repository.wheather.VolleyRequestManager
import kotlinx.android.synthetic.main.fragment_wheather.*
import kotlinx.android.synthetic.main.fragment_wheather.view.*
import java.util.*

class WheatherFragment : Fragment() {
    lateinit var root :View
    var defaultCityName : String ="indore"
    private var forecastsList: List<WeatherCelciousData.Forecasts> = java.util.ArrayList<WeatherCelciousData.Forecasts>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root= inflater.inflate(R.layout.fragment_wheather, container, false)
         root.foreCastRecyclerView.layoutManager= LinearLayoutManager(activity,RecyclerView.VERTICAL,false)


        callApi(defaultCityName)

        root.checkWeather.setOnClickListener {
            if (!TextUtils.isEmpty(root.editcityName.text.toString())) {
                mainContainer.visibility = View.GONE
                callApi(root.editcityName!!.text.toString())
                hideSoftKeyboard(editcityName)
            } else {
                editcityName.error = "कृपया शहर का नाम लिखें"
                Toast.makeText(activity, "कृपया शहर का नाम लिखें", Toast.LENGTH_LONG).show()
            }
        }


        return root
    }
    private fun callApi(cityName : String) {
        root.loader!!.visibility = View.VISIBLE
        forecastsList = java.util.ArrayList<WeatherCelciousData.Forecasts>()
        val requestManager: VolleyRequestManager = VolleyRequestManager.getInstance(activity)
        val request =
            VolleyRequest<String>(cityName,
                Request.Method.GET,
                null,
                null,
                Response.Listener<String> { response ->
                    root.loader!!.visibility = View.GONE
                    if (response != null) mainContainer!!.visibility = View.VISIBLE
                    Log.e("Json response", response.toString())
                    // WeatherFerahniteData weatherData = new Gson().fromJson(response.toString(), WeatherFerahniteData.class);
                    val weatherData: WeatherCelciousData =
                        Gson().fromJson(response.toString(), WeatherCelciousData::class.java)
                    forecastsList = weatherData.forecasts!!
                    val mAdapter = WeatherAdapter(forecastsList, activity)
                    foreCastRecyclerView!!.adapter = mAdapter
                    //Toast.makeText(getActivity(), weatherData.getLocation().getCity(), Toast.LENGTH_SHORT).show();
                    address.setText(
                        weatherData.location?.city
                            .toString() + ", " + weatherData.location
                            ?.region + ", " + weatherData.location?.country
                    )
                    humidity!!.text =
                        "" + weatherData.current_observation?.atmosphere?.humidity
                            .toString() + "%"
                    temp!!.text =
                        "" + weatherData.current_observation?.condition?.temperature
                            .toString() + " \u2103"
                    //statusCondition.setText(weatherData.getCurrent_observation().getCondition().getText());
                    statusCondition.setText(
                        weatherData.current_observation?.condition?.code?.let {
                            wheather(
                                it,
                                activity
                            )
                        }
                    )
                    wind!!.text = "" + weatherData.current_observation?.wind?.speed
                        .toString() + "km/h"
                    sunrise.setText(
                        weatherData.current_observation?.astronomy?.sunrise
                    )
                    sunset.setText(weatherData.current_observation?.astronomy?.sunset)
                    pressure!!.text =
                        "" + weatherData.current_observation?.atmosphere?.pressure
                            .toString() + "mb"
                    Log.e(
                        "pbdate ",
                        "" + weatherData.current_observation?.pubDate
                    )
                    val finalDate: String = DateHelper.formatRFC822(
                        Date(
                            weatherData.current_observation?.pubDate!! * 1000L
                        )
                    )
                    println("  =================================>>>>>       $finalDate")
                    updatedAt!!.text = finalDate
                },
                Response.ErrorListener {
                    root.loader!!.visibility = View.GONE
                    Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
                    // Add error handling here
                }
            )
        requestManager.addToRequestQueue(request)


    }
    private fun hideSoftKeyboard(input: EditText) {
        val imm =
            activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(input.windowToken, 0)
    }
}

