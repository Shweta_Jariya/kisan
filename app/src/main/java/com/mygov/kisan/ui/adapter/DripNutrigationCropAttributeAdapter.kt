package com.mygov.kisan.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.mygov.kisan.R
import com.mygov.kisan.ui.model.CropDripNutrigation


class DripNutrigationCropAttributeAdapter(
    val context: FragmentActivity?,
    val cropTypeList: List<CropDripNutrigation.Crop_attribute>?): RecyclerView.Adapter<DripNutrigationCropAttributeAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: DripNutrigationCropAttributeAdapter.ViewHolder, position: Int) {
      val cropData : CropDripNutrigation.Crop_attribute = cropTypeList?.get(position)!!

        holder.tvCropTitle?.text = cropData.title
        holder.tvCropValue?.text = cropData.value
        if(position % 2 == 0) {
            holder.rlCropBack.setBackgroundColor(Color.parseColor("#a5dfa2"))
        }else{
            holder.rlCropBack.setBackgroundColor(Color.parseColor("#bbe9b8"))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DripNutrigationCropAttributeAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_crop_drip_nutrigation_crop_attribute, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return cropTypeList?.size!!

    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvCropTitle = itemView.findViewById<TextView>(R.id.tvCropTitle)

        val tvCropValue = itemView.findViewById<TextView>(R.id.tvCropValue)
        val rlCropBack = itemView.findViewById<RelativeLayout>(R.id.rlCropBack)

    }
}
