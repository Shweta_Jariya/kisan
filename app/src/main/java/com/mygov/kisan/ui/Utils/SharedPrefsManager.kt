package com.mygov.kisan.utils

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import java.lang.reflect.Type

class SharedPrefsManager private constructor(appContext: Context) {
    private val prefs: SharedPreferences = appContext.getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE)

    val editor: SharedPreferences.Editor
        get() = prefs.edit()

    /**
     * Clears all data in SharedPreferences
     */
    fun clearPrefs() {
        val editor = prefs.edit()
        editor.clear()
        editor.commit()
    }

    fun removeKey(key: String) {
        prefs.edit().remove(key).commit()
    }

    fun containsKey(key: String): Boolean {
        return prefs.contains(key)
    }

    @JvmOverloads
    fun getString(key: String, defValue: String? = null): String? {
        return prefs.getString(key, defValue)
    }

    fun setString(key: String, value: String?) {
        val editor = prefs.edit()
        editor.putString(key, value)
        editor.apply()
    }


    @JvmOverloads
    fun getInt(key: String, defValue: Int = 0): Int {
        return prefs.getInt(key, defValue)
    }

    fun setInt(key: String, value: Int) {
        val editor = prefs.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    @JvmOverloads
    fun getLong(key: String, defValue: Long = 0L): Long {
        return prefs.getLong(key, defValue)
    }

    fun setLong(key: String, value: Long) {
        val editor = prefs.edit()
        editor.putLong(key, value)
        editor.apply()
    }

    @JvmOverloads
    fun getBoolean(key: String, defValue: Boolean = false): Boolean {
        return prefs.getBoolean(key, defValue)
    }

    fun setBoolean(key: String, value: Boolean) {
        val editor = prefs.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    @JvmOverloads
    fun getFloat(key: String, defValue: Float = 0f): Boolean {
        return getFloat(key, defValue)
    }

    fun setFloat(key: String, value: Float?) {
        val editor = prefs.edit()
        editor.putFloat(key, value!!)
        editor.apply()
    }


    /**
     * Persists an Object in prefs at the specified key, class of given Object must implement Model
     * interface
     *
     * @param key         String
     * @param modelObject Object to persist
     * @param <M>         Generic for Object
    </M> */
    fun <M : Model> setObject(key: String, modelObject: M) {
        val value =
            createJSONStringFromObject(
                modelObject
            )
        val editor = prefs.edit()
        editor.putString(key, value)
        editor.apply()
    }

    /**
     * Fetches the previously stored Object of given Class from prefs
     *
     * @param key                String
     * @param classOfModelObject Class of persisted Object
     * @param <M>                Generic for Object
     * @return Object of given class
    </M> */
    fun <M : Model> getObject(key: String, classOfModelObject: Class<M>): M? {
        val jsonData = prefs.getString(key, null)
        if (null != jsonData) {
            try {
                val gson = Gson()
                return gson.fromJson(jsonData, classOfModelObject)
            } catch (cce: ClassCastException) {
                Log.d(
                    TAG, "Cannot convert string obtained from prefs into collection of type " +
                            classOfModelObject.name + "\n" + cce.message
                )
            }

        }
        return null
    }

    /**
     * Persists a Collection object in prefs at the specified key
     *
     * @param key            String
     * @param dataCollection Collection Object
     * @param <C>            Generic for Collection object
    </C> */
    fun <C> setCollection(key: String, dataCollection: C) {
        val editor = prefs.edit()
        val value =
            createJSONStringFromObject(
                dataCollection
            )
        editor.putString(key, value)
        editor.apply()
    }

    /**
     * Fetches the previously stored Collection Object of given type from prefs
     *
     * @param key     String
     * @param typeOfC Type of Collection Object
     * @param <C>     Generic for Collection Object
     * @return Collection Object which can be casted
    </C> */
    fun <C> getCollection(key: String, typeOfC: Type): C? {
        val jsonData = prefs.getString(key, null)
        if (null != jsonData) {
            try {
                val gson = Gson()
                return gson.fromJson<C>(jsonData, typeOfC)
            } catch (cce: ClassCastException) {
                Log.d(
                    TAG, "Cannot convert string obtained from prefs into collection of type " +
                            typeOfC.toString() + "\n" + cce.message
                )
            }

        }
        return null
    }

    fun registerPrefsListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) {
        prefs.registerOnSharedPreferenceChangeListener(listener)
    }

    fun unregisterPrefsListener(

        listener: SharedPreferences.OnSharedPreferenceChangeListener
    ) {
        prefs.unregisterOnSharedPreferenceChangeListener(listener)
    }

    companion object {
        private val TAG = SharedPrefsManager::class.java.name
        private var uniqueInstance: SharedPrefsManager? = null

        /**
         * Throws IllegalStateException if this class is not initialized
         *
         * @return unique SharedPrefsManager instance
         */
        val instance: SharedPrefsManager
            get() {
                checkNotNull(uniqueInstance) { "SharedPrefsManager is not initialized, call initialize(applicationContext) " + "static method first" }
                return uniqueInstance as SharedPrefsManager
            }

        /**
         * Initialize this class using application Context,
         * should be called once in the beginning by any application Component
         *
         * @param appContext application context
         */
        fun initialize(appContext: Context?) {
            if (appContext == null) {
                throw NullPointerException("Provided application context is null")
            }
            if (uniqueInstance == null) {
                synchronized(SharedPrefsManager::class.java) {
                    if (uniqueInstance == null) {
                        uniqueInstance =
                            SharedPrefsManager(appContext)
                    }
                }
            }
        }

        private fun createJSONStringFromObject(`object`: Any?): String {
            val gson = Gson()
            return gson.toJson(`object`)
        }
    }

    interface Model
}
