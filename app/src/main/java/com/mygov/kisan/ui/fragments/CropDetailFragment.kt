package com.mygov.kisan.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.mygov.kisan.R
import com.mygov.kisan.ui.Utils.NetworkCheck
import com.mygov.kisan.ui.adapter.ViewPagerAdapter
import com.mygov.kisan.ui.viewmodel.CropDetailViewModel
import com.mygov.kisan.utils.Constant
import kotlinx.android.synthetic.main.fragment_crop_detail.view.*
import kotlinx.android.synthetic.main.fragment_crop_lists.view.progressBar


class CropDetailFragment : Fragment() {
    // TODO: Rename and change types of parameters
    var cropId : String?=null
    var cropImage : String?=null
    lateinit var root :View
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments!=null)
            cropId = arguments?.getString(Constant.CROPS_ID)
            cropImage = arguments?.getString(Constant.CROPS_IMAGE)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root= inflater.inflate(R.layout.fragment_crop_detail, container, false)


        //initializeProgress()

        if (NetworkCheck.isNetworkAvailable(context)) {

            apiCallCropList()
        }

        setUpViewPager()

        return root
    }

    private fun setUpViewPager() {
        val adapter = ViewPagerAdapter(requireContext(),childFragmentManager, cropId,cropImage)
        root.viewPager.adapter = adapter
        root.tabs.setupWithViewPager(root.viewPager)
    }

    private fun apiCallCropList() {
        Log.e("cropId",""+cropId)
        CropDetailViewModel.instance.getCropsDetail(cropId).observe(requireActivity(), Observer { cropDetail ->


        })
    }


    private fun initializeProgress() {
        CropDetailViewModel.instance.isLoading.observe(requireActivity(), Observer { isLoading->
            if (isLoading){
                root.progressBar.visibility= View.VISIBLE
            }else{
                root.progressBar.visibility= View.GONE
            }

        })
    }
}