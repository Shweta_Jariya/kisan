package com.mygov.kisan.ui.Utils

import android.content.Context
import com.mygov.kisan.R

object WheatherCondition {
    @JvmStatic
    fun dayName(name: String?, context: Context): String? {
        var day: String? = null
        when (name) {
            "Sun" -> day = context.resources.getString(R.string.sunday)
            "Mon" -> day = context.resources.getString(R.string.monday)
            "Tue" -> day = context.resources.getString(R.string.tuesday)
            "Wed" -> day = context.resources.getString(R.string.wednessday)
            "Thu" -> day = context.resources.getString(R.string.thrusday)
            "Fri" -> day = context.resources.getString(R.string.friday)
            "Sat" -> day = context.resources.getString(R.string.saturday)
            else -> {
            }
        }
        return day
    }

    @JvmStatic
    fun wheather(code: Int, context: Context?): String? {
        var day: String? = null
        when (code) {
            0 -> day = "बवंडर"
            1 -> day = "उष्णकटिबंधीय तूफान"
            2 -> day = "तूफान"
            3 -> day = "भयानक गर्जना"
            4 -> day = "गरज"
            5 -> day = "मिश्रित बारिश और बर्फ"
            6 -> day = "मिश्रित बारिश और ओले के साथ वर्षा"
            7 -> day = "मिश्रित बर्फ और ओले के साथ वर्षा"
            8 -> day = "जमा देने वाली हवा"
            9 -> day = "बूंदा बांदी"
            10 -> day = "बर्फ़ीली वर्षा"
            11 -> day = "बारिश"
            12 -> day = "बारिश"
            13 -> day = "बर्फ आंधी"
            14 -> day = "हल्की बर्फ़बारी"
            15 -> day = "उड़ती हुई बर्फ़"
            16 -> day = "हिमपात"
            17 -> day = "ओला"
            18 -> day = "ओले के साथ वर्षा"
            19 -> day = "धूल"
            20 -> day = "धूमिल"
            21 -> day = "धुन्ध"
            22 -> day = "धुएँ के रंग का"
            23 -> day = "धमकी से"
            24 -> day = "तूफानी"
            25 -> day = "सर्दी"
            26 -> day = "बादल"
            27 -> day = "ज्यादातर बादल (रात)"
            28 -> day = "ज्यादातर बादल (दिन)"
            29 -> day = "आंशिक रूप से बादल (रात)"
            30 -> day = "आंशिक रूप से बादल छाए रहेंगे (दिन)"
            31 -> day = "बिना बादल वाली (रात)"
            32 -> day = "धूप"
            33 -> day = "निष्पक्ष (रात)"
            34 -> day = "उचित (दिन)"
            35 -> day = "मिश्रित बारिश और ओलों"
            36 -> day = "गरम"
            37 -> day = "अलग थलग बादलों का गरजना"
            38 -> day = "अस्तव्यस्त आँधी"
            39 -> day = "बिखरी हुई बारिश (दिन)"
            40 -> day = "भारी वर्षा"
            41 -> day = "बिखरे हिमपात (दिन)"
            42 -> day = "भारी बर्फ"
            43 -> day = "बर्फानी तूफान"
            44 -> day = "उपलब्ध नहीं है"
            45 -> day = "बिखरी हुई बारिश (रात)"
            46 -> day = "बिखरी हुई बर्फ की बौछारें (रात)"
            47 -> day = "बिखरे हुए गरज के साथ छींटे"
            else -> {
            }
        }
        return day
    }
}