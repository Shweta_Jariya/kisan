package com.mygov.kisan.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.findafundiseeker.repository.ApiInterface
import com.mygov.kisan.ui.model.CropTypes
import com.mygov.kisan.utils.Constant
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class WebService private constructor() {
    internal val api: ApiInterface

    init {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
             .connectTimeout(5, TimeUnit.MINUTES) // connect timeout
            .writeTimeout(5, TimeUnit.MINUTES) // write timeout
            .readTimeout(5, TimeUnit.MINUTES)
            .addInterceptor(logging)// read timeout
            .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(Constant.HTTPS_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

        api = retrofit.create(ApiInterface::class.java)
    }


    fun getCropTypes(): LiveData<CropTypes> {
        val data = MutableLiveData<CropTypes>()
        api.getCropTypes().enqueue(object : Callback<CropTypes> {
            override fun onResponse(call: Call<CropTypes>, response: Response<CropTypes>) {
                simulateDelay()
                if (response.isSuccessful) {
                   // var success = AppController.gson?.fromJson(response.body().toString(), LoginResponse::class.java)
                    data.setValue(response.body())
                }
            }

            override fun onFailure(call: Call<CropTypes>, t: Throwable) {
                // TODO better error handling in part #2 ...
                data.setValue(null)
                Log.e("API error", t.toString())
            }
        })

        return data
    }








    private fun simulateDelay() {
        try {
            Thread.sleep(10)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

    companion object {
        private var projectRepository: WebService? = null

        val instance: WebService
            @Synchronized get() {
                if (projectRepository == null) {
                    if (projectRepository == null) {
                        projectRepository = WebService()
                    }
                }
                return projectRepository!!
            }
    }
}
