package com.mygov.kisan.ui.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CropTypes {
    @Expose
    @SerializedName("result")
    var result: List<Result>? =
        null

    @Expose
    @SerializedName("message")
    var message: String? = null

    @Expose
    @SerializedName("status")
    var status = false

    class Result {
        @Expose
        @SerializedName("item_image")
        var item_image: String? = null

        @Expose
        @SerializedName("title")
        var title: String? = null

        @Expose
        @SerializedName("id")
        var id = 0

    }
}