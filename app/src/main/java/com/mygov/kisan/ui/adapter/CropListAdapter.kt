package com.mygov.kisan.ui.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mygov.kisan.R
import com.mygov.kisan.ui.model.CropList


class CropListAdapter(
    val context: FragmentActivity?,
    val cropTypeList: List<CropList.Result>?,
    private val cropListInterface: CropListInterface
): RecyclerView.Adapter<CropListAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: CropListAdapter.ViewHolder, position: Int) {
      val cropData : CropList.Result = cropTypeList?.get(position)!!


        context?.let {
            Glide.with(it)
                .load(cropData.image)
                .into(holder.ivCropImage)
        }

        holder.tvCropName?.text = cropData.crop
        holder.tvCropName?.setBackgroundColor(Color.parseColor(cropData.bg_color))


        holder.cardCrop.setOnClickListener {
            cropListInterface.onClickCrop(cropData)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CropListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_crop_types, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return cropTypeList?.size!!

    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivCropImage = itemView.findViewById<ImageView>(R.id.ivCropImage)

        val tvCropName = itemView.findViewById<TextView>(R.id.tvCropName)
        val cardCrop = itemView.findViewById<CardView>(R.id.cardCrop)

    }
    interface CropListInterface{
        fun onClickCrop(cropData: CropList.Result)
    }
}
