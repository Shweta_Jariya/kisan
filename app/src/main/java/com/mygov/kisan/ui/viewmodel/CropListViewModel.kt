package com.mygov.kisan.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mygov.kisan.repository.WebService
import com.mygov.kisan.ui.model.CropList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CropListViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text
    internal var isLoading = MutableLiveData<Boolean>()

    fun getCropsList(crop_type : String?): LiveData<CropList> {
        isLoading.value = true
        val data = MutableLiveData<CropList>()
        WebService.instance.api.getCropsList(crop_type).enqueue(object : Callback<CropList> {
            override fun onResponse(call: Call<CropList>, response: Response<CropList>) {
                isLoading.value = false
                if (response.isSuccessful) {
                    data.setValue(response.body())
                } else {
                    data.value = null
                }
            }

            override fun onFailure(call: Call<CropList>, t: Throwable) {
                data.value = null
                isLoading.value = false
                Log.e("API error", t.toString())
            }
        })



        return data
    }

    companion object {
        private var projectRepository: CropListViewModel? = null

        //TODO No need to implement this singleton in Part #2 since Dagger will handle it ...
        val instance: CropListViewModel
            @Synchronized get() {
                if (projectRepository == null) {
                    if (projectRepository == null) {
                        projectRepository = CropListViewModel()
                    }
                }
                return projectRepository!!
            }
    }
}