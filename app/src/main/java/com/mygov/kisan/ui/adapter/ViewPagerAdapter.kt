package com.mygov.kisan.ui.adapter

import android.content.Context
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.mygov.kisan.R
import com.mygov.kisan.ui.fragments.BimariKitFragment
import com.mygov.kisan.ui.fragments.DripNutrigationFragment
import com.mygov.kisan.ui.fragments.SamanayJankariFragment
import com.mygov.kisan.utils.Constant
import com.mygov.kisan.utils.Constant.Companion.CROPS_IMAGE

class ViewPagerAdapter(var  context: Context,supportFragmentManager: FragmentManager,
                       var cropsId: String?,
                       var cropsImage: String?
) : FragmentStatePagerAdapter(supportFragmentManager) {

    private lateinit var mFragmentTitle : String
    private lateinit var mFragment : Fragment
    private val mFragmentTitleList = ArrayList<String>()

    override fun getItem(position: Int): Fragment {
           when(position) {
            0 -> mFragment= DripNutrigationFragment().apply {  arguments = bundleOf(Constant.CROPS_ID to cropsId, CROPS_IMAGE to cropsImage)}
            1 -> mFragment= SamanayJankariFragment().apply {  arguments = bundleOf(Constant.CROPS_ID to cropsId, CROPS_IMAGE to cropsImage)}
            2 -> mFragment= BimariKitFragment().apply {  arguments = bundleOf(Constant.CROPS_ID to cropsId, CROPS_IMAGE to cropsImage)}

        }
        return mFragment

    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        //return mFragmentTitleList[position]
        when(position) {
            0 -> mFragmentTitle= context.getResources().getString(R.string.drip)
            1 ->mFragmentTitle= context.getResources().getString(R.string.samanya_jankari)
            2 ->mFragmentTitle= context.getResources().getString(R.string.bimari_keet)

        }


        return mFragmentTitle
    }


}