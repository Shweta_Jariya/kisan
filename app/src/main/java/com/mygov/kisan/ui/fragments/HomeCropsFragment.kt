package com.mygov.kisan.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.mygov.kisan.R
import com.mygov.kisan.ui.Utils.GridItemDecoration
import com.mygov.kisan.ui.Utils.NetworkCheck
import com.mygov.kisan.ui.adapter.HomeCropListAdapter
import com.mygov.kisan.ui.model.CropList
import com.mygov.kisan.ui.viewmodel.CropListViewModel
import com.mygov.kisan.utils.Constant
import com.mygov.kisan.utils.Constant.Companion.DEFAULT_CROP_ID
import kotlinx.android.synthetic.main.fragment_home_crops.view.*

class HomeCropsFragment : Fragment(), HomeCropListAdapter.CropListInterface {
    lateinit  var root : View
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
         root = inflater.inflate(R.layout.fragment_home_crops, container, false)
         //root.recyclerViewList.layoutManager= activity?.let { LinearLayoutManager(it, RecyclerView.VERTICAL,false) }
        root.rvHomeCropsList.layoutManager= activity?.let { GridLayoutManager(it,3) }
        root.rvHomeCropsList.addItemDecoration(GridItemDecoration(10, 3))

        initializeProgress()
        if (NetworkCheck.isNetworkAvailable(context)) {
            apiCallCropList()
        }
        return root
    }
    private fun apiCallCropList() {
        CropListViewModel.instance.getCropsList(DEFAULT_CROP_ID).observe(requireActivity(), Observer { cropList ->
            val cropsList =cropList.result
            val adapter = HomeCropListAdapter(activity, cropsList,this)
            root.rvHomeCropsList.adapter = adapter

        })
    }
    private fun initializeProgress() {
        CropListViewModel.instance.isLoading.observe(requireActivity(), Observer { isLoading->
            if (isLoading){
                root.progressBar.visibility= View.VISIBLE
            }else{
                root.progressBar.visibility= View.GONE
            }

        })
    }

    override fun onClickCrop(cropData: CropList.Result) {
        val bundle= bundleOf(Constant.CROPS_ID to cropData.id.toString(), Constant.CROPS_IMAGE to cropData.image)
        findNavController().navigate(R.id.action_home_crop_list_to_crops_details ,bundle)
    }
}