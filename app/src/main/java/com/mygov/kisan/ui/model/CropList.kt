package com.mygov.kisan.ui.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CropList {
    @Expose
    @SerializedName("result")
    var result: List<Result>? =
        null

    @Expose
    @SerializedName("message")
    var message: String? = null

    @Expose
    @SerializedName("status")
    var status = false

    class Result {
        @Expose
        @SerializedName("bg_color")
        var bg_color: String? = null

        @Expose
        @SerializedName("image")
        var image: String? = null

        @Expose
        @SerializedName("type")
        var type: String? = null

        @Expose
        @SerializedName("crop")
        var crop: String? = null

        @Expose
        @SerializedName("id")
        var id = 0

    }
}