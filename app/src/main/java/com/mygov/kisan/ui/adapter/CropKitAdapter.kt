package com.mygov.kisan.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mygov.kisan.R
import com.mygov.kisan.ui.model.CropKit


class CropKitAdapter(
    val context: FragmentActivity?,
    val cropTypeList: List<CropKit.Kit>?,
    private val cropKitInterface: CropKitInterface
): RecyclerView.Adapter<CropKitAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: CropKitAdapter.ViewHolder, position: Int) {
      val cropData : CropKit.Kit = cropTypeList?.get(position)!!


        context?.let {
            Glide.with(it)
                .load(cropData.image)
                .into(holder.ivCropImage)
        }

        holder.tvCropName?.text = cropData.title


        holder.cardCrop.setOnClickListener {
            cropKitInterface.onClickCropKit(cropData)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CropKitAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_crop_kit_types, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return cropTypeList?.size!!

    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivCropImage = itemView.findViewById<ImageView>(R.id.ivCropImage)

        val tvCropName = itemView.findViewById<TextView>(R.id.tvCropName)
        val cardCrop = itemView.findViewById<CardView>(R.id.cardCrop)

    }
    interface CropKitInterface{
        fun onClickCropKit(cropData: CropKit.Kit)
    }
}
