package com.mygov.kisan.ui.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CropKit {
    @Expose
    @SerializedName("result")
    var result: Result? = null

    @Expose
    @SerializedName("message")
    var message: String? = null

    @Expose
    @SerializedName("status")
    var status = false

    class Result {
        @Expose
        @SerializedName("diseases")
        var diseases: List<Diseases>? = null

        @Expose
        @SerializedName("kit")
        var kit: List<Kit>? = null

    }

    class Diseases {
        @Expose
        @SerializedName("big_image")
        var big_image: String? = null

        @Expose
        @SerializedName("title")
        var title: String? = null

        @Expose
        @SerializedName("image")
        var image: String? = null

        @Expose
        @SerializedName("crop_name")
        var crop_name: String? = null

    }

    class Kit {
        @Expose
        @SerializedName("big_image")
        var big_image: String? = null

        @Expose
        @SerializedName("title")
        var title: String? = null

        @Expose
        @SerializedName("image")
        var image: String? = null

        @Expose
        @SerializedName("crop_name")
        var crop_name: String? = null

    }
}