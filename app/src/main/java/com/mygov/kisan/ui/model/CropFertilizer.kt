package com.mygov.kisan.ui.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CropFertilizer {
    @Expose
    @SerializedName("result")
    var result: List<Result>? =
        null

    @Expose
    @SerializedName("message")
    var message: String? = null

    @Expose
    @SerializedName("status")
    var status = false

    class Result {
        @Expose
        @SerializedName("big_image")
        var big_image: String? = null

        @Expose
        @SerializedName("reason")
        var reason: String? = null

        @Expose
        @SerializedName("image")
        var image: String? = null

        @Expose
        @SerializedName("crop_name")
        var crop_name: String? = null

    }
}