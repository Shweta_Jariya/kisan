package com.mygov.kisan.ui.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MasterData {
    @Expose
    @SerializedName("response")
    internal var response: List<Response?>? = null

    @Expose
    @SerializedName("message")
    internal var message: String? = null

    @Expose
    @SerializedName("status")
    internal var status = false

    fun getResponse(): List<Response?>? {
        return response
    }

    fun setResponse(response: List<Response?>?) {
        this.response = response
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getStatus(): Boolean {
        return status
    }

    fun setStatus(status: Boolean) {
        this.status = status
    }

    class Response {
        @Expose
        @SerializedName("services")
        var services: List<Services>? = null

        @Expose
        @SerializedName("name")
        var name: String? = null

        @Expose
        @SerializedName("average_reviews")
        var average_reviews: String? = null

        @Expose
        @SerializedName("average_rating")
        var average_rating: String? = null

        @Expose
        @SerializedName("city")
        var city: String? = null

        @Expose
        @SerializedName("state")
        var state: String? = null

        @Expose
        @SerializedName("country")
        var country: String? = null

        @Expose
        @SerializedName("register_datetime")
        var register_datetime: String? = null

        @Expose
        @SerializedName("profile_access")
        var profile_access: String? = null

        @Expose
        @SerializedName("is_active")
        var is_active: String? = null

        @Expose
        @SerializedName("username")
        var username: String? = null

        @Expose
        @SerializedName("email")
        var email: String? = null

        @Expose
        @SerializedName("created_datetime")
        var created_datetime: String? = null

        @Expose
        @SerializedName("pro_search_criteria")
        var pro_search_criteria: String? = null

        @Expose
        @SerializedName("instagram_url")
        var instagram_url: String? = null

        @Expose
        @SerializedName("linkedin_url")
        var linkedin_url: String? = null

        @Expose
        @SerializedName("pinterest_url")
        var pinterest_url: String? = null

        @Expose
        @SerializedName("googleplus_url")
        var googleplus_url: String? = null

        @Expose
        @SerializedName("twitter_url")
        var twitter_url: String? = null

        @Expose
        @SerializedName("facebook_url")
        var facebook_url: String? = null

        @Expose
        @SerializedName("is_office_services")
        var is_office_services: String? = null

        @Expose
        @SerializedName("is_travels_client")
        var is_travels_client: String? = null

        @Expose
        @SerializedName("is_remote_service")
        var is_remote_service: String? = null

        @Expose
        @SerializedName("work_ability")
        var work_ability: String? = null

        @Expose
        @SerializedName("is_available")
        var is_available: String? = null

        @Expose
        @SerializedName("about_me")
        var about_me: String? = null

        @Expose
        @SerializedName("job_tagline")
        var job_tagline: String? = null

        @Expose
        @SerializedName("company_name")
        var company_name: String? = null

        @Expose
        @SerializedName("end_hourly_rate")
        var end_hourly_rate: String? = null

        @Expose
        @SerializedName("hourly_rate")
        var hourly_rate: String? = null

        @Expose
        @SerializedName("image")
        var image: String? = null

        @Expose
        @SerializedName("mst_cities_id")
        var mst_cities_id: String? = null

        @Expose
        @SerializedName("mst_states_id")
        var mst_states_id: String? = null

        @Expose
        @SerializedName("mst_countries_id")
        var mst_countries_id: String? = null

        @Expose
        @SerializedName("zipcode")
        var zipcode: String? = null

        @Expose
        @SerializedName("location")
        var location: String? = null

        @Expose
        @SerializedName("gender")
        var gender: String? = null

        @Expose
        @SerializedName("dob")
        var dob: String? = null

        @Expose
        @SerializedName("phone")
        var phone: String? = null

        @Expose
        @SerializedName("last_name")
        var last_name: String? = null

        @Expose
        @SerializedName("first_name")
        var first_name: String? = null

        @Expose
        @SerializedName("user_id")
        var user_id: String? = null

        @Expose
        @SerializedName("business_started_date")
        var business_started_date: String? = null

        @Expose
        @SerializedName("is_online")
        var is_online: String? = null

        @Expose
        @SerializedName("longitude")
        var longitude: String? = null

        @Expose
        @SerializedName("payment_method_accepted")
        var payment_method_accepted: String? = null

        @Expose
        @SerializedName("latitude")
        var latitude: String? = null

        @Expose
        @SerializedName("is_pro_verified")
        var is_pro_verified: String? = null

        @Expose
        @SerializedName("role_id")
        var role_id: String? = null

    }

    class Services {
        @Expose
        @SerializedName("subcategory")
        var subcategory: String? = null

        @Expose
        @SerializedName("category")
        var category: String? = null

        @Expose
        @SerializedName("sub_category_id")
        var sub_category_id: String? = null

        @Expose
        @SerializedName("category_id")
        var category_id: String? = null

        @Expose
        @SerializedName("title")
        var title: String? = null

        @Expose
        @SerializedName("id")
        var id: String? = null

    }
}