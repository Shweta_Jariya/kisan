package com.mygov.kisan.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.mygov.kisan.R
import com.mygov.kisan.ui.model.CropDripNutrigation


class DripNutrigationDetailAdapter(
    val context: FragmentActivity?,
    val cropTypeList: List<CropDripNutrigation.Details>?
): RecyclerView.Adapter<DripNutrigationDetailAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: DripNutrigationDetailAdapter.ViewHolder, position: Int) {
      val cropData : CropDripNutrigation.Details = cropTypeList?.get(position)!!


        holder.tvCropDuration?.text =  context?.getResources()?.getString(R.string.ropan_k).toString() + " " + cropData.start_day + " " + context?.getResources()?.getString(R.string.se) + " " + cropData.end_day + " " + context?.getResources()?.getString(R.string.din_bad) + " - " + cropData.life_cycle




    /*    if (!cropData.fertilizers_urea.isNullOrEmpty()) holder.tvCropDuration?.text = String.format("%.2f",cropData.fertilizers_urea)
           else holder.tvCropDuration?.text ="0.0"*/

        if (!cropData.fertilizers_urea.isNullOrEmpty()) holder.tvUrea?.text =cropData.fertilizers_urea
        else holder.tvUrea?.text ="0.0"
        if (!cropData.h3po4.isNullOrEmpty()) holder.tvPhasporicAcid?.text = cropData.h3po4
        else holder.tvPhasporicAcid?.text ="0.0"
        if (!cropData.white_potas.isNullOrEmpty())  holder.tvWhitePotas?.text = cropData.white_potas
        else holder.tvWhitePotas?.text ="0.0"
        if (!cropData.fertilizers_amm_sulphate.isNullOrEmpty()) holder.tvAmoniumSalphet?.text = cropData.fertilizers_amm_sulphate
        else holder.tvAmoniumSalphet?.text ="0.0"
        if (!cropData.fertilizers_mgso4.isNullOrEmpty()) holder.tvMgso4?.text = cropData.fertilizers_mgso4
        else holder.tvMgso4?.text ="0.0"
        if (!cropData.ca3n2.isNullOrEmpty()) holder.tvCano3?.text = cropData.ca3n2
        else holder.tvCano3?.text ="0.0"

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DripNutrigationDetailAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_crop_drip_nutrigation_details, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return cropTypeList?.size!!

    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvUrea = itemView.findViewById<TextView>(R.id.tvUrea)
        val tvPhasporicAcid = itemView.findViewById<TextView>(R.id.tvPhasporicAcid)
        val tvWhitePotas = itemView.findViewById<TextView>(R.id.tvWhitePotas)
        val tvAmoniumSalphet = itemView.findViewById<TextView>(R.id.tvAmoniumSalphet)
        val tvMgso4 = itemView.findViewById<TextView>(R.id.tvMgso4)
        val tvCano3 = itemView.findViewById<TextView>(R.id.tvCano3)
        val tvCropDuration = itemView.findViewById<TextView>(R.id.tvCropDuration)

    }

    fun optToDouble(s: String): Double {
        return try {
            s.toDouble()
        } catch (e: Exception) {
            0.0
        }
    }
}
