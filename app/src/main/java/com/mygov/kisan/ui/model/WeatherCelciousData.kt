package com.mygov.kisan.ui.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WeatherCelciousData {
    @Expose
    @SerializedName("forecasts")
    var forecasts: List<Forecasts>? = null

    @Expose
    @SerializedName("current_observation")
    var current_observation: Current_observation? = null

    @Expose
    @SerializedName("location")
    var location: Location? = null

    class Forecasts {
        @Expose
        @SerializedName("code")
        var code = 0

        @Expose
        @SerializedName("text")
        var text: String? = null

        @Expose
        @SerializedName("high")
        var high = 0

        @Expose
        @SerializedName("low")
        var low = 0

        @Expose
        @SerializedName("date")
        var date = 0

        @Expose
        @SerializedName("day")
        var day: String? = null

    }

    class Current_observation {
        @Expose
        @SerializedName("pubDate")
        var pubDate = 0

        @Expose
        @SerializedName("condition")
        var condition: Condition? =
            null

        @Expose
        @SerializedName("astronomy")
        var astronomy: Astronomy? = null

        @Expose
        @SerializedName("atmosphere")
        var atmosphere: Atmosphere? = null

        @Expose
        @SerializedName("wind")
        var wind: Wind? = null

    }

    class Condition {
        @Expose
        @SerializedName("temperature")
        var temperature = 0

        @Expose
        @SerializedName("code")
        var code = 0

        @Expose
        @SerializedName("text")
        var text: String? = null

    }

    class Astronomy {
        @Expose
        @SerializedName("sunset")
        var sunset: String? = null

        @Expose
        @SerializedName("sunrise")
        var sunrise: String? = null

    }

    class Atmosphere {
        @Expose
        @SerializedName("rising")
        var rising = 0

        @Expose
        @SerializedName("pressure")
        var pressure = 0

        @Expose
        @SerializedName("visibility")
        var visibility = 0.0

        @Expose
        @SerializedName("humidity")
        var humidity = 0

    }

    class Wind {
        @Expose
        @SerializedName("speed")
        var speed = 0

        @Expose
        @SerializedName("direction")
        var direction = 0

        @Expose
        @SerializedName("chill")
        var chill = 0

    }

    class Location {
        @Expose
        @SerializedName("timezone_id")
        var timezone_id: String? = null

        @Expose
        @SerializedName("long")
        var longitute = 0.0

        @Expose
        @SerializedName("lat")
        var lat = 0.0

        @Expose
        @SerializedName("country")
        var country: String? = null

        @Expose
        @SerializedName("region")
        var region: String? = null

        @Expose
        @SerializedName("city")
        var city: String? = null

        @Expose
        @SerializedName("woeid")
        var woeid = 0

    }
}