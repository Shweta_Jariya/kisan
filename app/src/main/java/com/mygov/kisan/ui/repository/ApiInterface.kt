package com.findafundiseeker.repository

import com.google.gson.JsonObject
import com.mygov.kisan.ui.model.*
import retrofit2.Call
import retrofit2.http.*


internal interface ApiInterface {

    @GET("getCountries")
    fun getCountryCity(): Call<JsonObject>

    @POST("get_crop_types")
    fun getCropTypes(): Call<CropTypes>

    @FormUrlEncoded
    @POST("get_crops")
    fun getCropsList(@Field("crop_type") crop_type: String?): Call<CropList>

    @FormUrlEncoded
    @POST("get_crop_details")
    fun getCropsDetail(@Field("crop_id") crop_id: String?): Call<CropDripNutrigation>

    @FormUrlEncoded
    @POST("get_fertilizer_lack_symptoms_by_crop_id")
    fun getFertilizerOfCrop(@Field("crop_id") crop_id: String?): Call<CropFertilizer>

    @FormUrlEncoded
    @POST("get_plantation_chart")
    fun getCropChart(@Field("crop_id") crop_id: String?): Call<CropChart>


    @FormUrlEncoded
    @POST("get_kit_by_crop_id")
    fun getCropKit(@Field("crop_id") crop_id: String?): Call<CropKit>
}
