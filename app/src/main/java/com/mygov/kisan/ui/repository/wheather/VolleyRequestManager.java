package com.mygov.kisan.ui.repository.wheather;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyRequestManager {
    private static VolleyRequestManager sInstance;

    Context mContext;
    RequestQueue mRequestQueue;

    public static synchronized VolleyRequestManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new VolleyRequestManager(context);
        }
        return sInstance;
    }

    private VolleyRequestManager(Context context) {
        mContext = context;
        mRequestQueue = Volley.newRequestQueue(mContext);
    }

    public <T> void addToRequestQueue(Request<T> request) {
        mRequestQueue.add(request);
    }
}
