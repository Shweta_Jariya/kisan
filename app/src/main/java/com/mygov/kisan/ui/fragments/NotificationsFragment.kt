package com.mygov.kisan.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mygov.kisan.R
import com.mygov.kisan.ui.adapter.NotificationListAdapter
import com.mygov.kisan.ui.model.MasterData
import com.mygov.kisan.ui.viewmodel.NotificationsViewModel
import kotlinx.android.synthetic.main.fragment_notifications.view.*

class NotificationsFragment : Fragment(), NotificationListAdapter.EducationInterface {
    val masterList: List<MasterData.Response?>? = emptyList()
    lateinit var root: View
    private lateinit var notificationsViewModel: NotificationsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
            ViewModelProvider(this).get(NotificationsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_notifications, container, false)


        root.recyclerViewList.layoutManager = activity?.let { LinearLayoutManager(it, RecyclerView.VERTICAL, false) }







      /*  initializeProgress()
        if (NetworkCheck.isNetworkAvailable(context)) {
            val adapter = NotificationListAdapter(activity, masterList, this)
            root.recyclerViewList.adapter = adapter
            // apiCallMasterList()
        }*/
        return root
    }

   /* private fun initializeProgress() {
        NotificationsViewModel.instance.isLoading.observe(requireActivity(), Observer { isLoading ->
            if (isLoading) {
                root.llProgressBar.visibility = View.VISIBLE
            } else {
                root.llProgressBar.visibility = View.GONE
            }

        })
    }*/

    override fun onClickViewProfile(jobsData: MasterData.Response) {
        TODO("Not yet implemented")
    }
}