package com.mygov.kisan.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mygov.kisan.repository.WebService
import com.mygov.kisan.ui.model.CropTypes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text
    internal var isLoading = MutableLiveData<Boolean>()

    fun getCropTypes(): LiveData<CropTypes> {
        isLoading.value = true
        val data = MutableLiveData<CropTypes>()
        WebService.instance.api.getCropTypes().enqueue(object : Callback<CropTypes> {
            override fun onResponse(call: Call<CropTypes>, response: Response<CropTypes>) {
                isLoading.value = false
                if (response.isSuccessful) {
                    data.setValue(response.body())
                } else {
                    data.value = null
                }
            }

            override fun onFailure(call: Call<CropTypes>, t: Throwable) {
                data.value = null
                isLoading.value = false
                Log.e("API error", t.toString())
            }
        })



        return data
    }

    companion object {
        private var projectRepository: DashboardViewModel? = null

        //TODO No need to implement this singleton in Part #2 since Dagger will handle it ...
        val instance: DashboardViewModel
            @Synchronized get() {
                if (projectRepository == null) {
                    if (projectRepository == null) {
                        projectRepository = DashboardViewModel()
                    }
                }
                return projectRepository!!
            }
    }
}