package com.mygov.kisan.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mygov.kisan.R
import com.mygov.kisan.ui.model.CropKit


class CropBimariAdapter(
    val context: FragmentActivity?,
    val cropTypeList: List<CropKit.Diseases>?,
    private val cropBimariInterface: CropBimariInterface
): RecyclerView.Adapter<CropBimariAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: CropBimariAdapter.ViewHolder, position: Int) {
      val cropData : CropKit.Diseases = cropTypeList?.get(position)!!


        context?.let {
            Glide.with(it)
                .load(cropData.image)
                .into(holder.ivCropImage)
        }

        holder.tvCropName?.text = cropData.title

        holder.ivMore.setOnClickListener {
            cropBimariInterface.onClickCrop(cropData)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CropBimariAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_crop_bimari_types, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return cropTypeList?.size!!

    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivCropImage = itemView.findViewById<ImageView>(R.id.ivCropImage)

        val tvCropName = itemView.findViewById<TextView>(R.id.tvCropName)
        val ivMore = itemView.findViewById<ImageView>(R.id.ivMore)

    }
    interface CropBimariInterface{
        fun onClickCrop(cropData: CropKit.Diseases)
    }
}
