package com.mygov.kisan.ui.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CropChart {
    @Expose
    @SerializedName("result")
    var result: Result? = null

    @Expose
    @SerializedName("message")
    var message: String? = null

    @Expose
    @SerializedName("status")
    var status = false

    class Result {
        @Expose
        @SerializedName("plantation_chart")
        var plantation_chart: List<Plantation_chart>? = null

        @Expose
        @SerializedName("crop_economics")
        var crop_economics: List<Crop_economics>? = null

    }

    class Plantation_chart {
        @Expose
        @SerializedName("ideal_production")
        var ideal_production: String? = null

        @Expose
        @SerializedName("line_pattern")
        var line_pattern = 0

        @Expose
        @SerializedName("bed_size")
        var bed_size: String? = null

        @Expose
        @SerializedName("distance_between_rows_cm")
        var distance_between_rows_cm: String? = null

        @Expose
        @SerializedName("distance_between_columns_cm")
        var distance_between_columns_cm: String? = null

        @Expose
        @SerializedName("distance_between_plants_cm")
        var distance_between_plants_cm: String? = null

        @Expose
        @SerializedName("general_info")
        var general_info: String? = null

        @Expose
        @SerializedName("crop_name")
        var crop_name: String? = null

    }

    class Crop_economics {
        @Expose
        @SerializedName("average_price_unit")
        var average_price_unit: String? = null

        @Expose
        @SerializedName("average_price")
        var average_price: String? = null

        @Expose
        @SerializedName("average_production_unit")
        var average_production_unit: String? = null

        @Expose
        @SerializedName("average_production")
        var average_production: String? = null

        @Expose
        @SerializedName("average_expenditure")
        var average_expenditure: String? = null

    }
}