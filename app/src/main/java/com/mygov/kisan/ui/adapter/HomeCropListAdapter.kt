package com.mygov.kisan.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mygov.kisan.R
import com.mygov.kisan.ui.model.CropList


class HomeCropListAdapter(
    val context: FragmentActivity?,
    val cropTypeList: List<CropList.Result>?,
    private val cropListInterface: CropListInterface
): RecyclerView.Adapter<HomeCropListAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: HomeCropListAdapter.ViewHolder, position: Int) {
      val cropData : CropList.Result = cropTypeList?.get(position)!!


        context?.let {
            Glide.with(it)
                .load(cropData.image)
                .into(holder.ivCropImage)
        }

        holder.tvCropName?.text = cropData.crop


        holder.ivCropImage.setOnClickListener {
            cropListInterface.onClickCrop(cropData)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeCropListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_home_crops, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return cropTypeList?.size!!

    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivCropImage = itemView.findViewById<ImageView>(R.id.ivCropImage)

        val tvCropName = itemView.findViewById<TextView>(R.id.tvCropName)

    }
    interface CropListInterface{
        fun onClickCrop(cropData: CropList.Result)
    }
}
