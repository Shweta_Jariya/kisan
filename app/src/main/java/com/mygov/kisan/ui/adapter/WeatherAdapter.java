package com.mygov.kisan.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mygov.kisan.R;
import com.mygov.kisan.ui.Utils.DateHelper;
import com.mygov.kisan.ui.model.WeatherCelciousData;

import java.util.Date;
import java.util.List;

import static com.mygov.kisan.ui.Utils.WheatherCondition.dayName;
import static com.mygov.kisan.ui.Utils.WheatherCondition.wheather;


public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.MyViewHolder> {


    private List<WeatherCelciousData.Forecasts> forecastsList;
    private Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView day,date,tempMax,tempMin,overAll;

        public MyViewHolder(View view) {
            super(view);
            day=view.findViewById(R.id.day);
            date=view.findViewById(R.id.date);
            tempMax=view.findViewById(R.id.temp_max);
            tempMin=view.findViewById(R.id.temp_min);
            overAll=view.findViewById(R.id.overAll);


        }
    }


    public WeatherAdapter(List<WeatherCelciousData.Forecasts> moviesList, Context context) {
        this.forecastsList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.forecast_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        WeatherCelciousData.Forecasts forecasts = forecastsList.get(position);
        holder.day.setText(dayName(forecasts.getDay(),context));

        holder.tempMax.setText(forecasts.getHigh()+" \u2103");
        holder.tempMin.setText(forecasts.getLow()+" \u2103");

       // holder.overAll.setText(forecasts.getText());
        holder.overAll.setText(wheather(forecasts.getCode(),context));

        String finalDate = DateHelper.formatRFC822(new Date(forecasts.getDate() * 1000L));
        System.out.println("  =================================>>>>>       " + finalDate);
        holder.date.setText(finalDate);
    }


    @Override
    public int getItemCount() {
        return forecastsList.size();
    }
}