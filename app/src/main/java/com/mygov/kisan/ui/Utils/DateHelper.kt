package com.mygov.kisan.ui.Utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

 object DateHelper {
    private val TAG = DateHelper::class.java.simpleName
    const val DEFAULT = "MM/dd/yyyy hh:mm:ss a Z"
    const val ISO8601 = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ"
    const val ISO8601_NOMS = "yyyy-MM-dd'T'HH:mm:ssZ"
    const val RFC822 = "EEE, dd MMM yyyy hh:mm:ss a"
    const val SIMPLE = "MM/dd/yyyy hh:mm:ss a"
    fun format(format: String?, date: Date?): String {
        val sdf = SimpleDateFormat(format)
        return sdf.format(date)
    }

    fun format(date: Date?): String {
        return format(DEFAULT, date)
    }

    fun formatISO8601(date: Date?): String {
        return format(ISO8601, date)
    }

    fun formatISO8601NoMilliseconds(date: Date?): String {
        return format(ISO8601_NOMS, date)
    }
     @JvmStatic
    fun formatRFC822(date: Date?): String {
        return format(RFC822, date)
    }

    @Throws(ParseException::class)
    fun parse(date: String?): Date {
        return parse(DEFAULT, date)
    }

    @Throws(ParseException::class)
    fun parse(format: String?, date: String?): Date {
        val sdf = SimpleDateFormat(format)
        return sdf.parse(date)
    }

    @Throws(ParseException::class)
    fun parseISO8601(date: String?): Date {
        return parse(ISO8601, date)
    }

    @Throws(ParseException::class)
    fun parseISO8601NoMilliseconds(date: String?): Date {
        return parse(ISO8601_NOMS, date)
    }

    @Throws(ParseException::class)
    fun parseRFC822(date: String?): Date {
        return parse(RFC822, date)
    }

    fun Now(): Date {
        return Date()
    }
}