package com.mygov.kisan.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.mygov.kisan.R
import com.mygov.kisan.ui.Utils.GridItemDecoration
import com.mygov.kisan.ui.Utils.NetworkCheck
import com.mygov.kisan.ui.adapter.CropListAdapter
import com.mygov.kisan.ui.model.CropList
import com.mygov.kisan.ui.model.MasterData
import com.mygov.kisan.ui.viewmodel.CropListViewModel
import com.mygov.kisan.utils.Constant.Companion.CROPS_ID
import com.mygov.kisan.utils.Constant.Companion.CROPS_IMAGE
import kotlinx.android.synthetic.main.fragment_crop_lists.view.*


class CropListFragment : Fragment(), CropListAdapter.CropListInterface {
    var cropTypeId : String?=null
    val masterList: List<MasterData.Response?>? = emptyList()
    lateinit var root :View
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments!=null)
            cropTypeId = arguments?.getString(CROPS_ID)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root= inflater.inflate(R.layout.fragment_crop_lists, container, false)
        root.recyclerViewCropList.layoutManager= activity?.let { GridLayoutManager(it,2) }
        root.recyclerViewCropList.addItemDecoration(GridItemDecoration(10, 2))

       // root.recyclerViewCropList.layoutManager= activity?.let { LinearLayoutManager(it, RecyclerView.HORIZONTAL,false) }

        initializeProgress()

        if (NetworkCheck.isNetworkAvailable(context)) {

            apiCallCropList()
        }
        return root
    }

    private fun apiCallCropList() {
        Log.e("cropId",""+cropTypeId)
        CropListViewModel.instance.getCropsList(cropTypeId).observe(requireActivity(), Observer { cropList ->
             val cropsList =cropList.result
            val adapter = CropListAdapter(activity, cropsList,this)
                   root.recyclerViewCropList.adapter = adapter

        })
    }


    private fun initializeProgress() {
        CropListViewModel.instance.isLoading.observe(requireActivity(), Observer { isLoading->
            if (isLoading){
                root.progressBar.visibility= View.VISIBLE
            }else{
                root.progressBar.visibility= View.GONE
            }

        })
    }

    override fun onClickCrop(cropData: CropList.Result) {
        val bundle= bundleOf(CROPS_ID to cropData.id.toString(), CROPS_IMAGE to cropData.image)
        findNavController().navigate(R.id.action_crop_list_to_crops_details ,bundle)
    }

}