package com.mygov.kisan.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mygov.kisan.repository.WebService
import com.mygov.kisan.ui.model.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CropDetailViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text
    internal var isLoading = MutableLiveData<Boolean>()

    fun getCropsDetail(crop_type : String?): LiveData<CropDripNutrigation> {
        isLoading.value = true
        val data = MutableLiveData<CropDripNutrigation>()
        WebService.instance.api.getCropsDetail(crop_type).enqueue(object : Callback<CropDripNutrigation> {
            override fun onResponse(call: Call<CropDripNutrigation>, response: Response<CropDripNutrigation>) {
                isLoading.value = false
                if (response.isSuccessful) {
                    data.setValue(response.body())
                } else {
                    data.value = null
                }
            }

            override fun onFailure(call: Call<CropDripNutrigation>, t: Throwable) {
                data.value = null
                isLoading.value = false
                Log.e("API error", t.toString())
            }
        })



        return data
    }
    fun getCropChart(crop_type : String?): LiveData<CropChart> {
        isLoading.value = true
        val data = MutableLiveData<CropChart>()
        WebService.instance.api.getCropChart(crop_type).enqueue(object : Callback<CropChart> {
            override fun onResponse(call: Call<CropChart>, response: Response<CropChart>) {
                isLoading.value = false
                if (response.isSuccessful) {
                    data.setValue(response.body())
                } else {
                    data.value = null
                }
            }

            override fun onFailure(call: Call<CropChart>, t: Throwable) {
                data.value = null
                isLoading.value = false
                Log.e("API error", t.toString())
            }
        })



        return data
    }
    fun getFertilizerOfCrop(crop_type : String?): LiveData<CropFertilizer> {
        isLoading.value = true
        val data = MutableLiveData<CropFertilizer>()
        WebService.instance.api.getFertilizerOfCrop(crop_type).enqueue(object : Callback<CropFertilizer> {
            override fun onResponse(call: Call<CropFertilizer>, response: Response<CropFertilizer>) {
                isLoading.value = false
                if (response.isSuccessful) {
                    data.setValue(response.body())
                } else {
                    data.value = null
                }
            }

            override fun onFailure(call: Call<CropFertilizer>, t: Throwable) {
                data.value = null
                isLoading.value = false
                Log.e("API error", t.toString())
            }
        })



        return data
    }
    fun getCropKit(crop_type : String?): LiveData<CropKit> {
        isLoading.value = true
        val data = MutableLiveData<CropKit>()
        WebService.instance.api.getCropKit(crop_type).enqueue(object : Callback<CropKit> {
            override fun onResponse(call: Call<CropKit>, response: Response<CropKit>) {
                isLoading.value = false
                if (response.isSuccessful) {
                    data.setValue(response.body())
                } else {
                    data.value = null
                }
            }

            override fun onFailure(call: Call<CropKit>, t: Throwable) {
                data.value = null
                isLoading.value = false
                Log.e("API error", t.toString())
            }
        })



        return data
    }

    fun getCropsDripNutrigation(crop_type : String?): LiveData<CropDripNutrigation> {
        isLoading.value = true
        val data = MutableLiveData<CropDripNutrigation>()
        WebService.instance.api.getCropsDetail(crop_type).enqueue(object : Callback<CropDripNutrigation> {
            override fun onResponse(call: Call<CropDripNutrigation>, response: Response<CropDripNutrigation>) {
                isLoading.value = false
                if (response.isSuccessful) {
                    data.setValue(response.body())
                } else {
                    data.value = null
                }
            }

            override fun onFailure(call: Call<CropDripNutrigation>, t: Throwable) {
                data.value = null
                isLoading.value = false
                Log.e("API error", t.toString())
            }
        })



        return data
    }
    companion object {
        private var projectRepository: CropDetailViewModel? = null

        //TODO No need to implement this singleton in Part #2 since Dagger will handle it ...
        val instance: CropDetailViewModel
            @Synchronized get() {
                if (projectRepository == null) {
                    if (projectRepository == null) {
                        projectRepository = CropDetailViewModel()
                    }
                }
                return projectRepository!!
            }
    }
}