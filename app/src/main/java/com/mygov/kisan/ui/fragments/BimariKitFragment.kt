package com.mygov.kisan.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mygov.kisan.R
import com.mygov.kisan.ui.Utils.GridItemDecoration
import com.mygov.kisan.ui.Utils.NetworkCheck
import com.mygov.kisan.ui.adapter.CropBimariAdapter
import com.mygov.kisan.ui.adapter.CropKitAdapter
import com.mygov.kisan.ui.model.CropKit
import com.mygov.kisan.ui.viewmodel.CropDetailViewModel
import com.mygov.kisan.utils.Constant
import kotlinx.android.synthetic.main.fragment_bimari_kit.view.*
import kotlinx.android.synthetic.main.fragment_bimari_kit.view.ivCropImg
import kotlinx.android.synthetic.main.fragment_bimari_kit.view.progressBar

class BimariKitFragment : Fragment(), CropBimariAdapter.CropBimariInterface,
    CropKitAdapter.CropKitInterface {
     lateinit var root : View
    var cropId : String?=null
    var cropImage : String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments!=null)
            cropId = arguments?.getString(Constant.CROPS_ID)
          cropImage = arguments?.getString(Constant.CROPS_IMAGE)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root=inflater.inflate(R.layout.fragment_bimari_kit, container, false)
        root.recyclerViewBimari.layoutManager= activity?.let { LinearLayoutManager(it, RecyclerView.VERTICAL,false) }
        root.recyclerViewKit.layoutManager= activity?.let { GridLayoutManager(it, 2) }
        root.recyclerViewKit.addItemDecoration(GridItemDecoration(10, 2))


        initializeProgress()

        if (NetworkCheck.isNetworkAvailable(context)) {

            Log.e("cropId",cropId.toString())
            apiCallCropData()
        }


        return root
    }
    private fun apiCallCropData() {
        CropDetailViewModel.instance.getCropKit(cropId.toString())
            .observe(requireActivity(), Observer { cropBimari ->
                if (cropBimari != null) {
                    val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
                    Glide.with(requireActivity()).load(cropImage).apply(requestOptions).into(root.ivCropImg)
                    root.tvCropTitle.text= cropBimari?.result?.diseases?.get(0)?.crop_name

                    if (cropBimari.result?.diseases?.size!=0){
                        val adapter = CropBimariAdapter(activity, cropBimari.result?.diseases,this)
                        root.recyclerViewBimari.adapter = adapter
                    }
                    if (cropBimari.result?.kit?.size!=0){
                        val kitAdapter = CropKitAdapter(activity, cropBimari.result?.kit,this)
                        root.recyclerViewKit.adapter = kitAdapter
                    }



                }
            })
    }



    private fun initializeProgress() {
        CropDetailViewModel.instance.isLoading.observe(requireActivity(), Observer { isLoading->
            if (isLoading){
                root.progressBar.visibility= View.VISIBLE
            }else{
                root.progressBar.visibility= View.GONE
            }

        })
    }

    override fun onClickCrop(cropData: CropKit.Diseases) {

    }

    override fun onClickCropKit(cropData: CropKit.Kit) {

    }
}
