package com.mygov.kisan.utils

class Constant {

    companion object {
        const val SP_NAME = "SP_KISAN"
        const val CROPS_ID = "crops_id"
        const val CROPS_IMAGE = "crops_img"
        const val DEFAULT_CROP_ID = "15"
        const val HTTPS_API_BASE_URL = "https://laravel.cppatidar.com/samadhan/webservices/"

    }


}