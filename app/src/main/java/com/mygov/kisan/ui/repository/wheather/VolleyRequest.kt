package com.mygov.kisan.ui.repository.wheather

import com.android.volley.AuthFailureError
import com.android.volley.NetworkResponse
import com.android.volley.ParseError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonRequest
import com.google.gson.JsonSyntaxException
import net.oauth.OAuth
import net.oauth.OAuthAccessor
import net.oauth.OAuthConsumer
import net.oauth.OAuthMessage
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.util.*


class VolleyRequest<T>(
    cityName: String,
    method: Int,
    url: String?,
    requestBody: String?,
    listener: Response.Listener<String>,
    errorListener: Response.ErrorListener?
) : JsonRequest<String>(method, url, requestBody, listener, errorListener) {
    val appId = "gNdlOe4q"

    // final String appId = "RkWHAV70";
    val CONSUMER_KEY =
        "dj0yJmk9NHM5U2FEc2cwMlAzJmQ9WVdrOVowNWtiRTlsTkhFbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTk4"

    // final String CONSUMER_KEY = "dj0yJmk9VzVOd0UxNTkzbDhNJmQ9WVdrOVVtdFhTRUZXTnpBbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PWNj";
    val CONSUMER_SECRET = "f6d5100299962d8c8f6e5276304e0f3d2d358e99"

    //final String CONSUMER_SECRET = "6fa84abe4d7ebf7bbb3672afbb0645d7fbff757d";
    val baseUrl = "https://weather-ydn-yql.media.yahoo.com/forecastrss"
    var cityName = ""

    @Throws(AuthFailureError::class)
    override fun getHeaders(): Map<String, String> {
        val headers: MutableMap<String, String> =
            HashMap()
        val consumer = OAuthConsumer(null, CONSUMER_KEY, CONSUMER_SECRET, null)
        consumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.HMAC_SHA1)
        val accessor = OAuthAccessor(consumer)
        try {
            val request = accessor.newRequestMessage(OAuthMessage.GET, url, null)
            val authorization = request.getAuthorizationHeader(null)
            headers["Authorization"] = authorization
        } catch (e: Exception) {
            throw AuthFailureError(e.message)
        }
        headers["X-Yahoo-App-Id"] = appId
        headers["Content-Type"] = "application/json"
        return headers
    }

    override fun getUrl(): String {
        return "$baseUrl?location=$cityName,cn&format=json&u=c"
    }

    override fun parseNetworkResponse(response: NetworkResponse): Response<String> {
        return try {
           // val json = String(response.data , HttpHeaderParser.parseCharset(response.headers))

            val json = String(
                response?.data ?: ByteArray(0),
                Charset.forName(HttpHeaderParser.parseCharset(response?.headers)))
            val parsedResponse = parseResponse(json)
            Response.success(
                json, HttpHeaderParser.parseCacheHeaders(response))
        } catch (e: UnsupportedEncodingException) {
            Response.error(ParseError(e))
        } catch (e: JsonSyntaxException) {
            Response.error(ParseError(e))
        }
    }

/*
    override fun parseNetworkResponse(response: NetworkResponse?): Response<T> {
        return try {
            val json = String(
                response?.data ?: ByteArray(0),
                Charset.forName(HttpHeaderParser.parseCharset(response?.headers)))
            Response.success(
                gson?.fromJson(json, clazz),
                HttpHeaderParser.parseCacheHeaders(response))
        }catch (e: UnsupportedEncodingException) {
            Response.error(ParseError(e))
        } catch (e: JsonSyntaxException) {
            Response.error(ParseError(e))
        }
    }

*/




    private fun parseResponse(jsonObject: String): T? {
        return null // Add response parsing here
    }

    init {
        this.cityName = cityName
    }


}